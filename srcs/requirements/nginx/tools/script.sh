apt-get update
apt-get install -y nginx
mkdir -p /etc/nginx/ssl_certs/
mv /private.pem /etc/nginx/ssl_certs/
mv /public.key /etc/nginx/ssl_certs/
mv default /etc/nginx/sites-available/default
